"""
Python-Gnip
"""
from setuptools import setup


setup(
    name='Python-Gnip',
    version='0.1.0',
    url='http://code.benjie.me/python-gnip/',
    license='BSD',
    author='Benjie Jiao',
    author_email='benjiao12@gmail.com',
    description='Adds support to Gnip API',
    long_description=__doc__,
    py_modules=['pygnip'],
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=[],
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
