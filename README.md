# Python Gnip
A python wrapper for the Gnip API.

### Installation
```
git clone https://gitlab.com/benjiao12/python-gnip.git
cd python-gnip
python setup.py build
python setup.py install
```

### Usage

#### PowerTrack API

Initialization
```
from pygnip import GnipPowerTrack

gnip = GnipPowerTrack(account_name="account-name",
                      username="yourusername",
                      password="yourpassword")
```

----
Add Rule
```
gnip.addRule(tag="yourcustomtag",
             value="Filter OR Filters")
```

----
Remove Rule
```
gnip.removeRule(rule="Filter OR Filters")
```
